/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author steven
 */
final class PanelWrapper  extends JFrame
                    implements AppFactory.App {
    
    AppFactory.App appPanel;
    
    PanelWrapper(AppFactory.App app) {
        this(app, 400, 500);
    }
    
    PanelWrapper(AppFactory.App app, int width, int height) {
        super();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        add((JPanel)app);
        setSize(width, height);
        setVisible(true);
        
        appPanel = app;
    }

    @Override
    public void init() {
        appPanel.init();
    }

    @Override
    public void exec() {
        appPanel.exec();
    }
}
