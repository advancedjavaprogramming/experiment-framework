/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author steven
 */


public class AppFactory {
    
    private static AppFactory instance = null;
    
    public interface App {
        void init();
        void exec();
    }
    
    private AppFactory() {
        
    }
    
    public static AppFactory getInstance() {
        if(instance == null) {
            instance = new AppFactory();
        }
        return instance;
    }
    
    private HashMap<String, Class<? extends JFrame>> classMap = new HashMap<>();
    
    void registerCreator(String registeredName, Class<? extends JFrame> klass) {
        if(classMap.get(registeredName) == null) {
            if(AppFactory.App.class.isAssignableFrom(klass)) {
                classMap.put(registeredName, klass);
            }
            else {
                System.err.printf("'%s' does not implement the AppFactory.App interface\n", registeredName);
            }
        }
        else {
            System.err.printf("'%s' is already a registered creator\n", registeredName);
        }
    }
    
    void unregisterCreator(String registeredName) {
        if(classMap.get(registeredName) != null) {
            classMap.remove(registeredName);
        }
        else {
            System.err.printf("'%s' is not registered creator\n", registeredName);
        }
    }
    
    JFrame create(String creatorName) {
        Class<? extends JFrame> klass = classMap.get(creatorName);
        if(klass != null) {
            try {
                JFrame jf = klass.getConstructor().newInstance();
                
                return jf;
                
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(AppFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
              
        }
        return null;
    }
    
    List<String> getRegisteredCreators() {
        List<String> names = new ArrayList<>();
        
        for(String key : classMap.keySet()) {
            names.add(key);
        }
        
        Collections.sort(names);
        
        return Collections.unmodifiableList(names);
    }
}
