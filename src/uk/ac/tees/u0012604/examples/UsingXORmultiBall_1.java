package uk.ac.tees.u0012604.examples;

import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

public final class UsingXORmultiBall_1    extends JFrame
                                          implements AppFactory.App {
    // globals

    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // draw a bunch of stuff first
        drawSomeGraphics(g);

        int[] y = {50, 100, 150};		// y coords for 3 balls
        int size = 20;		// ball size
        Color ballColor = Color.RED;
        int step = 15;		// distance ball moves each time slice

        g.setXORMode(ballColor);
        // not how it's getting complicated even with a v.simple model
        for (int x = 50; x < FRAME_WIDTH - 50; x += step) {
            for (int b = 0; b < y.length; b++) {
                g.fillOval(x, y[b], size, size);		// draw them once
            }
            Utils.pause(400);
            for (int b = 0; b < y.length; b++) {
                g.fillOval(x, y[b], size, size);		// draw them again
            }
        }
    }

    private void drawSomeGraphics(Graphics g) {	// just for demo purposes, this draws a bunch of stuff
        // to emphasise a problem

        int dx = FRAME_WIDTH / 10;
        int dy = FRAME_HEIGHT / 10;

        boolean b = true;
        for (int x = 0, y = 0; x < FRAME_WIDTH / 2; b = !b, x += dx, y += dy) {
            if (b) {
                g.setColor(Color.LIGHT_GRAY);
                g.fillOval(x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y);
            } else {
                g.setColor(Color.GRAY);
                g.fillRect(x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y);
            }
        }
    }

    public UsingXORmultiBall_1() {
        super();

        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setVisible(true);
        repaint();
    }

    @Override
    public void init() {
        // Not used in this example.
    }

    @Override
    public void exec() {
        // Not used in this example.
    }
}
