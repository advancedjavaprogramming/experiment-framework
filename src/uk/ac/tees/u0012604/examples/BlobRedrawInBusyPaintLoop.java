package uk.ac.tees.u0012604.examples;

import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

public final class BlobRedrawInBusyPaintLoop    extends JFrame
                                                implements AppFactory.App
{   
    // globals
    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;

    // making some of these instance vars this time
    // to keep code simpler
	
    int x = 50;			// x needs to be global here

    public void paint(Graphics g)
    {	super.paint(g);

            // draw a bunch of stuff first
            drawSomeGraphics(g);

            final int y = 100;		// y coord
            final int size = 20;		// ball size
            final Color ballColor = Color.RED;
            final int step = 15;		// distance ball moves each time slice

            g.setColor( ballColor );
            g.fillOval( x, y, size, size );
            if( x < FRAME_WIDTH - 50 )
            {	
                    x += step;
                    Utils.pause(200);		// keep it slow to see what's happening
                    repaint();				// this recursive calling of repaint is
            }							// no a great idea, my excuse here is that
                                                                    // I'm demonstrating a point
    }

    private void drawSomeGraphics( Graphics g )
    {	
        // just for demo purposes, this draws a bunch of stuff
        // to emphasise a problem

        int dx = FRAME_WIDTH / 10;
        int dy = FRAME_HEIGHT / 10;

        boolean b = true;

        for(int x=0, y=0; x < FRAME_WIDTH / 2; b = !b, x += dx, y += dy)
        {	
            if(b)
            {	g.setColor( Color.LIGHT_GRAY );
                    g.fillOval( x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y );
            }
            else
            {	g.setColor( Color.GRAY );
                    g.fillRect( x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y );
            }
        }
    }
	
    public BlobRedrawInBusyPaintLoop()
    {   
        super();
           
        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Blob Redraw in Busy Paint Loop");
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setVisible(true);
    }

    @Override
    public void init() {
        /* Not used in this example */
    }

    @Override
    public void exec() {
        /* Not used in this example */
    }
}
