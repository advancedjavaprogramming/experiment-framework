package uk.ac.tees.u0012604.examples;

import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

public final class UsingXORsimple   extends JFrame
                                    implements AppFactory.App {
    // globals

    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;

    @Override
    public void paint(Graphics g) {
        // draw a bunch of stuff first
        drawSomeGraphics(g);

        int y = 100;		// y coord
        int size = 20;		// ball size
        Color ballColor = Color.RED;
        int step = 15;		// distance ball moves each time slice

        g.setXORMode(ballColor);
        for (int x = 50; x < FRAME_WIDTH - 50; x += step) {
            g.fillOval(x, y, size, size);		// draw it once
            Utils.pause(200);
            g.fillOval(x, y, size, size);		// draw it again (to erase it)
        }
    }

    private void drawSomeGraphics(Graphics g) {	// just for demo purposes, this draws a bunch of stuff
        // to emphasise a problem

        int dx = FRAME_WIDTH / 10;
        int dy = FRAME_HEIGHT / 10;

        boolean b = true;
        for (int x = 0, y = 0; x < FRAME_WIDTH / 2; b = !b, x += dx, y += dy) {
            if (b) {
                g.setColor(Color.LIGHT_GRAY);
                g.fillOval(x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y);
            } else {
                g.setColor(Color.GRAY);
                g.fillRect(x, y, FRAME_WIDTH - 2 * x, FRAME_HEIGHT - 2 * y);
            }
        }
    }

    public UsingXORsimple() {
        super();

        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setVisible(true);
    }
    
    
    @Override
    public void init() {
        // Not used in this example.
    }

    @Override
    public void exec() {
        // Not used in this example.
    }
}
