/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604.examples;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

/**
 *
 * @author u0012604
 */
public final class BlobGraphicsInPaintLoop1   extends JFrame
                                    implements AppFactory.App
{   
	// globals
	private final int FRAME_WIDTH = 500;
	private final int FRAME_HEIGHT = 300;
	
	
	public void paint(Graphics g)
	{	   	
		final int y = 100;		// y coord
		final int size = 20;		// ball size
		final Color ballColor = Color.RED;
		final int step = 15;		// distance ball moves each time slice
		
		g.setColor(ballColor);
                
		for(int x = 50; x < FRAME_WIDTH - 50; x += step)
		{	
                    g.fillOval(x, y, size, size);
                    Utils.pause(200);		// keep it slow to see what's happening
		}
	}

    public BlobGraphicsInPaintLoop1()
    {   
        super();
           
        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setTitle("Graphics in Paint Loop 1");
        setVisible(true);
    }

    @Override
    public void init() {
        /* not used in this example */
    }

    @Override
    public void exec() {
        /* not used in this example */
    }
}
