/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604.examples;

import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author steven
 */
class BlobPanel extends JPanel {
    
    public interface BlobPanelStrategy {
        public void init(ArrayList<Blob> blobs);
        public void exec(ArrayList<Blob> blobs);
    }
    
    private static BlobPanelStrategy noOpStrategy = new BlobPanelStrategy() {
        
        @Override
        public void init(ArrayList<Blob> blobs) {
            /* Do Nothing!! */
        }
        
        @Override
        public void exec(ArrayList<Blob> blobs) {
            /* Do Nothing!! */
        }        
    };
    
    BlobPanelStrategy blobPanelStrategy;
    
    ArrayList<Blob> blobs = new ArrayList<>();
        
    public BlobPanel() {
        this.setBounds(0, 0, 500, 500);
        
        
        blobPanelStrategy = noOpStrategy;
           
    }
    
    public void setStrategy(BlobPanelStrategy newStrategy) {
        blobPanelStrategy = newStrategy;
    }
    
    public void init() {
        blobPanelStrategy.init(blobs);
        
        System.out.println("Total Blobs: " + blobs.size());
    }
    
    public void exec() {
        //delegate to the strategy
        blobPanelStrategy.exec(blobs);
    }
        
    public void paint(Graphics g) {
        super.paint(g);
        for(Blob b : blobs) {
            b.draw(g);
        }
    }
}
