/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.tees.u0012604.examples;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

class BlobPaintingPanelStrategy implements BlobPanel.BlobPanelStrategy {
    
    BlobPanel blobPanel;
    Graphics panelGraphics;

    BlobPaintingPanelStrategy(BlobPanel blobPanel) {
        this.blobPanel = blobPanel;
    }
    
    
    @Override
    public void init(ArrayList<Blob> blobs) {
        for(int x = 0; x < 500; x += 100) {
            for(int y = 0; y < 500; y += 100) {
                blobs.add(new Blob(x, y));
            }         
        }
    }
     
    @Override
    public void exec(ArrayList<Blob> blobs) {
        //Threads will have uncontrolled access to
        //this panel's graphics object.
        //
        panelGraphics = blobPanel.getGraphics();
        
        Thread redThread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int blobIdx = 0;
                        Random random = new Random();
                        while(blobPanel.isShowing()) {
                            
                            blobIdx = random.nextInt(blobs.size());

                            //System.out.println("Red Thread: " + blobIdx);

                            Blob blob = blobs.get(blobIdx);

                            blob.setColor(Color.RED);

                            blob.draw(panelGraphics);
                                
                            Utils.pause(200);
                        }
                        
                        System.out.println("Red Thread Finished");
                    }
                    
                }
        );
        
        Thread blueThread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int blobIdx;
                        Random random = new Random();
                        while(blobPanel.isShowing()) {
                            
                            blobIdx = random.nextInt(blobs.size());

                            //System.out.println("Blue Thread: " + blobIdx);

                            Blob blob = blobs.get(blobIdx);

                            blob.setColor(Color.BLUE);

                            blob.draw(panelGraphics);
                                
                            Utils.pause(200);

                        }
                        
                        System.out.println("Blue Thread Finished");
                    }
                    
                }
        );
        
        Thread yellowThread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int blobIdx;
                        Random random = new Random();
                        while(blobPanel.isShowing()) {
                            
                            blobIdx = random.nextInt(blobs.size());

                            //System.out.println("Yellow Thread: " + blobIdx);

                            Blob blob = blobs.get(blobIdx);

                            blob.setColor(Color.YELLOW);

                            blob.draw(panelGraphics);
                                
                            Utils.pause(200);
                            
                        }
                        
                        System.out.println("Yellow Thread Finished");
                    }
                    
                }
        );
                
        Thread repaintThread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        final int FRAME_INTERVAL = 1000 / 24;
                        while(blobPanel.isShowing()) {
                            blobPanel.repaint();
                            
                           Utils.pause(FRAME_INTERVAL);
                        }
                    }
                }
        );
        
        redThread.start();
        blueThread.start();
        yellowThread.start();
        
        repaintThread.start();    
    }
    
}

/**
 *
 * @author steven
 */
public class BlobPainting   extends javax.swing.JFrame
                            implements AppFactory.App {

    /**
     * Creates new form MainFrame
     */
    BlobPanel blobPanel;
    
    public BlobPainting() {
        initComponents();
        blobPanel = new BlobPanel();
        blobPanel.setStrategy(new BlobPaintingPanelStrategy(blobPanel));
        this.add(blobPanel);

    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public void start(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BlobPainting.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BlobPainting.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BlobPainting.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BlobPainting.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


    }

    @Override
    public void init() {
        setSize(500,550);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Blob Painting");
        
        blobPanel.init();
    }

    @Override
    public void exec() {
        
        //The panel's where the action's at!
        blobPanel.exec();

    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
