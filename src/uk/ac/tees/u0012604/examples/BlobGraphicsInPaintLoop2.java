/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604.examples;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

/**
 *
 * @author u0012604
 */
public final class BlobGraphicsInPaintLoop2 extends JFrame
                                            implements AppFactory.App
{   
    // globals
    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;


    public void paint(Graphics g)
    {	   	
        final int size = 20;		// ball size
        final Color ballColor = Color.RED;
        final Color background = getBackground();
        final int step = 15;		// distance ball moves each time slice
        final int y = 100;		// y coord

        // different ways to organise the loop this does 1st draw
        // before the loop then erase & redraw inside
        int x = 50;
        g.setColor(ballColor);				// set foreground color
        g.fillOval(x, y, size, size);			// draw blob

        for(; x < FRAME_WIDTH - 50; x += step)
        {	
            Utils.pause(200);					// pause
            g.setColor(background);			// set background color
            g.fillOval(x, y, size, size);		// erase blob
            g.setColor(ballColor);			// set foreground color
            g.fillOval(x + step, y, size, size);		// draw blob
        }
    }

    public BlobGraphicsInPaintLoop2()
    {   
        super();
           
        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setVisible(true);
    }

    @Override
    public void init() {
        /* not used in this example */
    }

    @Override
    public void exec() {
        /* not used in this example */
    }
}
