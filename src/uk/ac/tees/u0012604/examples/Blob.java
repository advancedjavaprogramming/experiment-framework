/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604.examples;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author steven
 */
class Blob {
    int x, y, radius;
    Color color;
    
    Blob(int x, int y) {
        this(x, y, 100);
    }
    
    Blob(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    
    public void draw(Graphics g) {
        erase(g);
        g.setColor(color);
        g.fillOval(x, y, 100, 100);
        g.setColor(Color.BLACK);
    }
    
    public void setColor(Color c) {
        color = c;
    }
    
    public int getX() { return x; }
    
    public int getY() { return y; }
    
    public void setX(int x) { this.x = x; }
    
    public void setY(int y) { this.y = y; }
    
    private void erase(Graphics g) {
        g.clearRect(x, y, 100, 100);
    }
}
