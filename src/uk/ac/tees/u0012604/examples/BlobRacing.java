/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604.examples;

import java.util.ArrayList;
import javax.swing.JFrame;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

/**
 *
 * @author steven
 */
class BlobRacingPanelStrategy implements BlobPanel.BlobPanelStrategy {

    final static int THREAD_PAUSE_INTERVAL = 1000 / 24;
    
    private int newX;
    
    BlobPanel blobPanel;
    
    BlobRacingPanelStrategy(BlobPanel blobPanel) {
        this.blobPanel = blobPanel;
    }
    
    @Override
    public void init(ArrayList<Blob> blobs) {
        blobs.add(new Blob(50, 100));
        blobs.add(new Blob(70, 200));
        blobs.add(new Blob(90, 300));
    }

    @Override
    public void exec(final ArrayList<Blob> blobs) {
        ArrayList<Thread> threads = new ArrayList<>();
        
        for(Blob b : blobs) {
            Thread t = new Thread(
                    new Runnable() {
                        Blob myBlob = b;
                        
                        @Override
                        public void run() {
                            
                            while(blobPanel.isShowing()) {
                                newX = myBlob.getX() + 10;
                                
                                Utils.pause(200);
                                
                                myBlob.setX(newX);
                            }
                        }
                    }
            );
            
            threads.add(t);
        }
        
        threads.add(
                new Thread(
                        new Runnable() {
                            @Override
                            public void run() {
                                while(blobPanel.isShowing()) {
                                    blobPanel.repaint();
                                    
                                    Utils.pause(THREAD_PAUSE_INTERVAL);
                                }
                            }
                        }
                )
        );
        
        //start all the threads
        for(Thread t : threads)
            t.start();
        
    }
    
}

public class BlobRacing extends JFrame
                        implements AppFactory.App {

    BlobPanel blobPanel;
    
    @Override
    public void init() {
        setSize(500,550);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        blobPanel = new BlobPanel();
        
        add(blobPanel);
        
        blobPanel.setStrategy(new BlobRacingPanelStrategy(blobPanel));
        blobPanel.init();
    }

    @Override
    public void exec() {
        blobPanel.exec();
    }
    
}
