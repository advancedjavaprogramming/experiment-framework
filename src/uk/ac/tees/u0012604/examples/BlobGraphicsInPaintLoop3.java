package uk.ac.tees.u0012604.examples;

import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import uk.ac.tees.u0012604.AppFactory;
import uk.ac.tees.u0012604.utils.Utils;

public class BlobGraphicsInPaintLoop3   extends JFrame
                                        implements AppFactory.App
{   
    // globals
    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;
    private int size = 20;			// ball size
    private int x, y;
    private Graphics g;
    private Color ballColor = Color.RED;
	
	
    public void paint(Graphics g)
    {	
        this.g = g;   	
        final int step = 15;		// distance ball moves each time slice

        // situation changed by structuring a bit & using clearRect
        // but note it screws up back ground if you have graphics
        // on it, also you need more globals - choices!
        x = 50;
        y = 100;
        display();

        while( x < FRAME_WIDTH - 50 )
        {	
            Utils.pause(200);					// pause
            erase();
            x += step;
            display();
        }
    }

    public BlobGraphicsInPaintLoop3()
    {   super();
           
        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setTitle("Blob Graphics in Constructor 3");
        setVisible(true);
    }
    
    private void erase()
    {	
        g.clearRect( x, y, size, size );
    }
    
    private void display()
    {
        g.setColor( ballColor );
        g.fillOval( x, y, size, size );
    }

    @Override
    public void init() {
        /* Not used in this example */
    }

    @Override
    public void exec() {
        /* Not used in this example */
    }
}
